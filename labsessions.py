#import gi
#gi.require_version('Gtk', '3.0')
#from gi.repository import Gtk
#
#class LabSession(Gtk.Window):
#
#    def __init__(self):
#        Gtk.Window.__init__(self, title="LabSessions")
#        self.button = Gtk.Button(label="Init")
#        self.button.connect("clicked", self.on_button_clicked)
#        self.add(self.button)
#
#
#    def on_button_clicked(self, widget):
#        print("Initialized")
#
#
#if __name__ == "__main__":
#    win = LabSession()
#    win.connect("destroy", Gtk.main_quit)
#    win.show_all()
#    Gtk.main()



#!/usr/bin/env python3
import gtk 
import gobject
import os
from subprocess import Popen, PIPE
import fcntl

wnd = gtk.Window()
wnd.set_default_size(400, 400)
wnd.connect("destroy", gtk.main_quit)
label = gtk.Label()
label.set_alignment(0, 0)
wnd.add(label)
wnd.show_all()
sub_proc = Popen("ping -c 10 localhost", stdout=PIPE, shell=True)
sub_outp = ""


def non_block_read(output):
    ''' even in a thread, a normal read with block until the buffer is full '''
    fd = output.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    try:
        return output.read().decode("utf-8")
    except:
        return ''


def update_terminal():
    label.set_text(label.get_text() + non_block_read(sub_proc.stdout))
    return sub_proc.poll() is None

gobject.timeout_add(100, update_terminal)
gtk.main()
