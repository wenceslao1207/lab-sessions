#! /bin/bash

vboxmanage startvm "$1" & 
sleep 15
PID=$(pidof VirtualBox)

trap "kill $PID 2> /dev/null" EXIT
#KEYBOARD=$(getKeyboardUUID.py keyboard)
#MOUSE=$(getKeyboardUUID.py mouse)
/usr/bin/usb.py "$1" &
while kill -0 $PID 2> /dev/null; do
	#/usr/bin/usb.sh "$1" "$KEYBOARD" "$MOUSE"
	sleep 3
done

systemctl poweroff
