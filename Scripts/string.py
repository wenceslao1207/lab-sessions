#! /usr/bin/env python

import sys
string = sys.argv[1]

def checkComilla():
	output = ""
	for char in string:
		if char == "'":
			output += "\\"
			output += "'"
		else:
			output += char
	return output

if __name__ == "__main__":

	print(checkComilla())


