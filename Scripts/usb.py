#!/usr/bin/env python3

import subprocess
import time
import re
import sys


# Global
USB = {}
USBPASSED = []
VM = None

# Regular Expressions
UUID = re.compile('UUID*')
KEYBOARD = 'Keyboard'
MOUSE = 'Mouse'


class UsbDevice:
    def __init__(self):
        self.name = None
        self.UUID = None
        self.currentState = None

    def setFromCommmand(self, cmdOut):
        usbArray = cmdOut.split("\n")
        uuid= usbArray[0].split(" ")
        self.UUID = uuid[len(uuid)-1]
        state = usbArray[len(usbArray) -1]
        self.currentState = state[19:len(state)-1]
        try:
            name = usbArray[7]
            self.name = name[20:len(name)]
        except:
            self.name = "Anonimato"

def checkRemoved():
    for device in USBPASSED:
        if device not in USB:
            USBPASSED.remove(device)


def checkUsb():
    usbHost = subprocess.check_output(['vboxmanage', 'list', 'usbhost'])
    usbHost = usbHost.decode("utf-8")
    listUsbHost = usbHost.split('\n\n')
    for element in listUsbHost:
        newUsb = UsbDevice()
        newUsb.setFromCommmand(element)
        if not (newUsb.UUID in USB):
            USB[newUsb.UUID] = newUsb
    return USB

def passUsb(vm, USB):
    for device in USB:
        if USB[device].name != "Anonimato":
            if device not in USBPASSED:
                if (KEYBOARD not in USB[device].name) and (
                                                MOUSE not in USB[device].name):
                    command = "vboxmanage controlvm {} usbattach {}".format(
                                                                 vm, device)
                    passthrough = subprocess.Popen(command, shell=True)
                    USBPASSED.append(device)
    USB = {}
    return USB, USBPASSED

# Cargo el array con todos los dispositivos USB
if __name__ == "__main__":
    print("TESTING")
    while (True):
        try:
            VM = sys.argv[1]
        except:
            sys.exit(1)
        USB = checkUsb()
        checkRemoved()
        USB, USBPASSED = passUsb(VM, USB)
        time.sleep(5)
