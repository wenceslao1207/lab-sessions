#! /usr/bin/env bash

USERNAME="xp"

installVboxExt () {
	echo "[INSTALLING] Extention pack installed for vbox version "$VERSION
	echo "" 
	yes | VBoxManage extpack install --replace "$EXTENSION" 1> /dev/null 
	echo "[DONE] Extention pack installed for vbox version "$VERSION
	echo ""
}

checkVBoxVersion () {
	VERSION=$(vboxmanage --version | tail -n 1 | cut -c-6)
	LASTCHAR=${VERSION: -1}
	if [[ "$LASTCHAR" == "_" ]]; then
		VERSION=$(echo $VERSION | cut -c-5)
	fi
	EXTENSION="../Extensions/Oracle_VM_VirtualBox_Extension_Pack-"$VERSION".vbox-extpack"
	if [[ -f "$EXTENSION" ]]; then
		installVboxExt
	else
		echo "[WARNING] File not found... it'll start downloading now"
		URL="https://download.virtualbox.org/virtualbox/"$VERSION"/Oracle_VM_VirtualBox_Extension_Pack-"$VERSION".vbox-extpack"
		wget $URL -p "../Extensions/"
		echo "[DONE] Download completed"
 
		EXTENSION="download.virtualbox.org/virtualbox/"$VERSION"/Oracle_VM_VirtualBox_Extension_Pack-"$VERSION".vbox-extpack"
		installVboxExt
	fi
}

registerVM() {
	# Find Virtual Hard Drive and assign permissions
	echo "[SEARCHING] Virtual Drive"
	echo ""
	FILE=$(find /home/administrador/ -name winxp.VHD -print 2> /dev/null)
	echo "[FOUND] Virtual Drive in "$FILE
	echo ""
	chmod +775 "$FILE"

	# Find virtual machine and assign permissions
	echo "[SEARCHING] Virtual Machine"
	echo ""
	FILE=$(find /home/administrador/ -name xp.vbox -print 2> /dev/null)
	echo "[FOUND] Virtual Machine in "$FILE
	chmod +775 "$FILE"
	echo ""
	FILE=$(python ./string.py $FILE)
	chmod +775 /home/administrador/.config/VirtualBox/* 

	echo "[REGISTRATION] Virtual Machine for user: "$USERNAME
	echo ""
	COMMAND='"vboxmanage registervm '$FILE'"'
	su - xp -c "vboxmanage registervm $FILE" 2> /dev/null
	echo "[DONE] Registration"
}

checkUserName() {
	USER=$(getent passwd | grep -ow $USERNAME | tail -1)
	if [[ "$USER" == "" ]]; then
		echo "[USER] Creating user "$USERNAME
		useradd -d /home/$USERNAME $USERNAME
		yes $USERNAME | passwd $USERNAME 1> /dev/null
		echo "[DONE] User Created"
	fi
}

checkHomeDir() {
	DIRECTORY=/home/$USERNAME
	if [[ ! -d "$DIRECTORY" ]]; then
		mkdir $DIRECTORY
		chown $USERNAME $DIRECTORY 
	fi
}

checkGroups() {
	USERGROUPS=$(groups $USERNAME | grep vboxusers)
	if [[ "$USERGROUPS" == "" ]]; then
		echo "[GROUPS] Adding "$USERNAME" to Group vboxusers"
		usermod -g vboxusers $USERNAME
		echo "[DONE]"
	fi
}

placeScripts() {
	cp ./sessionManager.sh /usr/bin/
	cp ./vmstarter.sh /usr/bin
	cp ./usb.py /usr/bin
	cp ./lab.desktop /usr/share/xsessions/lab.desktop
}

changeMod() {
	chmod +x /usr/bin/sessionManager.sh
	chmod +x /usr/bin/vmstarter.sh
	chmod +x /usr/bin/usb.py
}

clear
checkUserName
checkHomeDir
checkGroups
placeScripts
changeMod
checkVBoxVersion
registerVM
