# LAB-SESSIONS
 
Lab-Sessions it's a project that came to life from the need to take care of the virtual XP machines in the different labs of the University. The problem was that some students kept turning the host off  without turning off the VM's first, and this constant action corrupted the virtual hard-drive.
So the idea of making this, is that a New-Session starts the VM when you log in, and as well as turn the host off when the VM does too.


### How to run

Run as superuser the file main.sh in the Scripts folder and pass the string "xp" as parameter 


```bash
# Make it executable
	 chmod +x main.sh
# First Method (using sudo)
	 sudo ./main.sh
# Second Method (using su)
	su -c './main.sh'

```

After that got to the login screen and change the session to term

### What it does:

**checkUserName**

Checks if the user $USER (in this case xp) already exists, if the user doesn't exist then it's created with th password being teh same as the username 

```bash
	USER=$(getent passwd | grep -ow $USERNAME | tail -1)
	if [[ "$USER" == "" ]]; then
		useradd -d /home/$USERNAME $USERNAME
		yes $USERNAME | passwd $USERNAME 1> /dev/null
	fi

```

**checkHomeDir**

Checks if the user talked before has a home directory already, if it doesn't then its created

```bash
 	DIRECTORY=/home/$USERNAME
	if [[ ! -d "$DIRECTORY" ]]; then
		mkdir $DIRECTORY
		chown $USERNAME $DIRECTORY 
	fi
```

**checkGroups**

Checks if the user is part of the vboxusers group 

```bash
	USERGROUPS=$(groups $USERNAME | grep vboxusers)
	if [[ "$USERGROUPS" == "" ]]; then
		usermod -g vboxusers $USERNAME
	fi
```

**placeScripts**

```bash
	cp ./sessionManager.sh /usr/bin/
	cp ./vmstarter.sh /usr/bin
	cp ./usb.sh /usr/bin
	cp ./getKeyboardUUID.py /usr/bin/
	cp ./lab.desktop /usr/share/xsessions/
```

**changeMod**

```bash
	chmod +x /usr/bin/sessionManager.sh
	chmod +x /usr/bin/vmstarter.sh
	chmod +x /usr/bin/usb.sh
	chmod +x /usr/bin/getKeyboardUUID.py
```

**checkVBoxVersion**

Checks VirtualBox's version and install the extension pack for that version

```bash
	VERSION=$(vboxmanage --version | tail -n 1 | cut -c-6)
	LASTCHAR=${VERSION: -1}
	if [[ "$LASTCHAR" == "_" ]]; then
		VERSION=$(echo $VERSION | cut -c-5)
	fi
	EXTENSION="../Extensions/Oracle_VM_VirtualBox_Extension_Pack-"$VERSION".vbox-extpack"
	if [[ -f "$EXTENSION" ]]; then
		installVboxExt
	else
		URL="https://download.virtualbox.org/virtualbox/"$VERSION"/Oracle_VM_VirtualBox_Extension_Pack-"$VERSION".vbox-extpack"
		wget $URL -p "../Extensions/"
		installVboxExt
	fi
```

**registerVM**

Registers the xp VM so its usable by the new user

```bash
	FILE=$(find / -name winxp.VHD -print 2> /dev/null)
	chmod +775 "$FILE"

	FILE=$(find / -name xp.vbox -print 2> /dev/null)
	chmod +775 "$FILE"
	
	FILE=$(python ./string.py $FILE)
	chmod +775 /home/administrador/.config/VirtualBox/* 

	COMMAND='"vboxmanage registervm '$FILE'"'
	su - xp -c "vboxmanage registervm $FILE" 2> /dev/null

```

